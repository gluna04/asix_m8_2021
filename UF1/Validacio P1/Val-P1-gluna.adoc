//Exemple de títol
= Validacio Practica 1
//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

//Exemple de subtítol

== Exercici 1

--
Configuracio netplan 1ª maquina.
--
image::images/1.PNG[]

--
Configuracio netplan 2ª maquina.
--
image::images/2.PNG[]

--
Comanda ip a 1ª maquina
--
image::images/3.PNG[]

--
Comanda ip a 2ª maquina.
--
image::images/4.PNG[]

--
Ping
--
image::images/5.PNG[]

--
Ping
--
image::images/6.PNG[]

--
Comanda ip route show 
--
image::images/7.PNG[]

--
Comanda ip route show 
--
image::images/8.PNG[]



== Exercici 2

--
Configuracio netplan 1ª maquina.
--
image::images/ex2_1.PNG[]

--
Configuracio netplan 2ª maquina.
--
image::images/ex2_2.PNG[]

--
Comanda ip a 
--
image::images/ex2_11.PNG[]

--
Comanda ip a 
--
image::images/ex2_3.PNG[]

--
Comanda ip a
--
image::images/ex2_4.PNG[]

--
Comanda ip route show 
--
image::images/ex2_5.PNG[]

--
Comanda ip route show 
--
image::images/ex2_6.PNG[]

--
Comanda ip route show 
--
image::images/ex2_7.PNG[]

--
Ping
--
image::images/ex2_8.PNG[]

--
Ping
--
image::images/ex2_9.PNG[]

--
Ping
--
image::images/ex2_10.PNG[]

== Exercici 3

--
Configuracio netplan.
--
image::images/ex3_1.PNG[]

--
Comanda ip a
--
image::images/ex3_2.PNG[]

--
Comanda ip route show
--
image::images/ex3_3.PNG[]

--
Ping
--
image::images/ex3_4.PNG[]

--
Ping a google
--
image::images/ex3_5.PNG[]