= Practica Email
//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:


== Configuracio del servidor DNS a la xarxa local

-Fitxer named.conf.local

image::images/1.png[]

-Fitxer de la zona

image::images/2.png[]

-Comprovacio que la zona funciona correctament

image::images/3.png[]

== Preparacio de la maquina Amy_wong

-Fitxer 00.yaml

image::images/4.png[]

== Instal·lador del servidor de correu a Amy_wong

-Proces d'instal·lacio

image::images/5.png[]

image::images/6.png[]

image::images/7.png[]

-Captura systemctl status

image::images/8.png[]

-Comanda postconf

image::images/9.png[]

== Comprovacio de la configuracio en local

-Captura de la seqüencia d'enviament i lectura del correu

image::images/10.png[]

== Comprovacio del servidor de correu via telnet

-Missatge rebut a Amy_wong

image::images/11.png[]


