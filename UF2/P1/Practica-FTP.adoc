= Servidor FTP
//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:


== Instal·lador del servidor FTP a Fry

-Transaccio FTP entre el client i Fry

image::images/1.png[]

== Configuracio de l'acces anonim

-Captura de la sortida de ls -ld

image::images/2.png[]

-Modificacions a la configuracio

image::images/3.png[]

-Transaccio utilitzant l'acces anonim des de l'ordinador fisic

image::images/4.png[]

== Separacio de la configuracio del FTP en xarxes

-Canvis al fitxer de configuracio

image::images/5.png[]

image::images/6.png[]

image::images/7.png[]

-Fitxer /etc/hosts.allow

image::images/8.png[]

-Comprovacio acces exterior

image::images/9.png[]

-Comprovacio acces intern

image::images/10.png[]




